import * as vscode from 'vscode';
export function activate(context: vscode.ExtensionContext) {
	let removeLineBreaks = vscode.commands.registerCommand('remove-break-lines.removeLineBreaks', () => {
		const activeTextEditor = vscode.window.activeTextEditor;
		function rmLnBr(text: string) {
			let newtext = text.replace(/(\r\n|\n|\r)/gm, " ");
			return newtext.replace(/\s+/g, " ");
		}
		if (activeTextEditor) {
			if (activeTextEditor.selections.length > 1) {
				activeTextEditor.edit(editBuilder => {
					activeTextEditor.selections.forEach(selection => {
						let text = activeTextEditor.document.getText(selection);
						let newtext = rmLnBr(text);
						editBuilder.replace(selection, newtext);
					})
				})
			} else {
				const selection = activeTextEditor.selection;
				if (!selection.isEmpty) {
					let text = activeTextEditor.document.getText(selection);
					let newtext = rmLnBr(text);
					activeTextEditor.edit(editBuilder => {
						editBuilder.replace(selection, newtext);
					})
				}
			}
		}
	});
	let removeEmptyLine = vscode.commands.registerCommand('remove-empty-lines.removeEmptyLines', () => {
		const activeTextEditor = vscode.window.activeTextEditor;
		function rmEpLn(text: string) {
			text = text.replace(/\r\n/g, '\n');
			for (var e = text.split("\n"), t = "", r = 0; r < e.length; r++) {
				var o = e[r];
				/^[\s\t]*$/.test(o) || (t += o + "\n")
			}
			return t
		}
		if (activeTextEditor) {
			if (activeTextEditor.selections.length > 1) {
				activeTextEditor.edit(editBuilder => {
					activeTextEditor.selections.forEach(selection => {
						let text = activeTextEditor.document.getText(selection);
						let newtext = rmEpLn(text);
						editBuilder.replace(selection, newtext);
					})
				})
			} else if (!activeTextEditor.selection.isEmpty) {
				const selection = activeTextEditor.selection;
				let text = activeTextEditor.document.getText(selection);
				let newtext = rmEpLn(text);
				activeTextEditor.edit(editBuilder => {
					editBuilder.replace(selection, newtext);
				})
			} else {
				let doc = activeTextEditor.document;
				let text = doc.getText();
				let newtext = rmEpLn(text);
				activeTextEditor.edit(editBuilder => {
					editBuilder.replace(new vscode.Range(doc.lineAt(0).range.start, doc.lineAt(doc.lineCount - 1).range.end), newtext);
				})
			}
		}
	});
	context.subscriptions.push(removeLineBreaks);
	context.subscriptions.push(removeEmptyLine);
}
export function deactivate() { }