# Change Log

All notable changes to the "remove-break-lines" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.


## [V 0.0.7]
- Command to remove line breaks.
- Multiple selections.



## [V 0.0.9]
- Command to removes empty lines.
- Multiple selections.
- Document selection.



## [V 1.0.0]
- Stable and better documented version..