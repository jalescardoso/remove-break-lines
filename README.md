# Remove Final/Empty lines

A [Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=jalescardoso.remove-break-lines) extension that manipulates lines.

## Usage

### Select a part or multiple parts of the document that you want to remove line breaks.

```cmd
CMD/CTRL + Shift + P -> Remove line breaks
```

### Select a part, multiple parts or the document you want to remove empty lines.

```cmd
CMD/CTRL + Shift + P -> Remove empty lines
```

## License

Licensed under the MIT License.
